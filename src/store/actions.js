const actions = {
  action({ commit, state }, data)
  {
    commit('MUTATION', data)
  }
}

export { actions }
