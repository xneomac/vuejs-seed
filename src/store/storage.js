import localforage from 'localforage'
require('localforage-startswith')

const APP = 'APP-';

const fetch = () =>
{
  return localforage.startsWith(APP).then(res =>
  {
    return res;
  })
}

const save = (data_id, data) =>
{
  return localforage.setItem(
    APP + data_id,
    data
  ).then(value =>
  {
    return value
  }).catch(err =>
  {
    console.log('oops, we lost your data.')
  })
}

const remove = (data_id) =>
{
  return localforage.removeItem(
    APP + data_id
  ).then(() =>
  {
    return true
  }).catch(err =>
  {
    return false
  })
}

export { fetch, save, remove };
