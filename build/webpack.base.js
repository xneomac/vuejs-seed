const path = require("path");
const webpack = require('webpack')

module.exports = {
  entry: './src/index.js',
  watch: false,
  watchOptions: {},
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, '../dist/assets'),
    publicPath: '/assets/'
  },
  resolve: {
    extensions: ['.js', '.vue', '.css', '.json'],
    alias: {
      root: path.join(__dirname, 'src'),
      components: path.join(__dirname, 'src/components')
    }
  },
  plugins: [],
  module: {
      loaders: [
          {
              test: /\.scss$/,
              loaders: ['style-loader', 'css-loader', 'sass-loader']
          },
          {
              test: /\.css$/,
              loader: ['style-loader', 'css-loader']
          },
          {
              test: /\.vue$/,
              loaders: ['vue-loader']
          },
          {
              test: /\.js$/,
              loaders: ['babel-loader'],
              exclude: [/node_modules/]
          }
      ]
  }
};
